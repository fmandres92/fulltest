const express = require('express');
const router = express.Router();

const charactersCtrl = require('../controllers/characters');

router.get('/getAll', charactersCtrl.getAll);

module.exports = router;
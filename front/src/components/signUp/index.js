import React, { useState } from "react";
import { Link } from "react-router-dom";
import {ToastsContainer, ToastsStore} from 'react-toasts';
import PropTypes from 'prop-types';
import Api from '../Api'
 
const SignUp = (props) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = () => {
    props.apiClient.registry({username,password}).then( (resp) => {
      const token = resp.data.token;
      localStorage.setItem('token', token);
      props.handleIn();
      props.history.push('/home')
    }).catch((err) => {
        if(err.status === 400) {
          ToastsStore.error(err.data.message)
        } else {
          ToastsStore.error('Sorry something went bad')
        }
    })
  }

  return(
    <form>
        <h3>Sign Up</h3>

        <div className="form-group">
            <label>User</label>
            <input type="text" value={username} onChange={(e) => setUsername(e.target.value) } className="form-control" placeholder="Username" />
        </div>

        <div className="form-group">
            <label>Password</label>
            <input type="password" value={password} onChange={(e) => setPassword(e.target.value) } className="form-control" placeholder="Enter password" />
        </div>

        <button type="button" className="btn btn-primary btn-block" onClick={() => handleSubmit()}>Sign Up</button>
        <p className="forgot-password text-right">
            <Link className="nav-link" to={"/sign-in"}> Already registered <b>sign in? </b> </Link>
        </p>
        <ToastsContainer store={ToastsStore}/>
    </form>
  )
}

SignUp.propTypes = {
  history: PropTypes.any,
  apiClient: PropTypes.shape(),
  handleIn: PropTypes.func
};

SignUp.defaultProps = {
    apiClient: new Api()
}

export default SignUp;

const jwt = require('jsonwebtoken');
const SECRET = '01010101'

module.exports = {
    async validateToken(token) {
        return new Promise((resolve,reject) => {
            try {
                const state = jwt.verify(token, SECRET);
                resolve(state)
            } catch (err) {
               reject(err)
            }
        })
    },
    async setToken(data) {
        return new Promise((resolve,reject) => {
            try {
                const token = jwt.sign({ data }, SECRET, { expiresIn: 120 });
                resolve(token)
            } catch (err) {
               reject(err)
            }
        })
    }
}
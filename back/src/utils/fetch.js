const request = require('request');

class Characters {
    constructor(){
        this.url = 'https://rickandmortyapi.com/api/character/';
    }

    fetch() {
        return new Promise((resolve,reject) => {
            request(this.url, (error, response, body) => {
                if(error) {
                   reject(error)
                }
                let { results } = JSON.parse(body);
                results = results.map((item) =>({
                    id: item.id,
                    name: item.name,
                    status: item.status,
                    species: item.species,
                    gender: item.gender,
                    image: item.image
                }))
                resolve(results);
            })
        })
    }
}

module.exports = new Characters() 
import React, {Fragment,useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";

import Login from "./components/login";
import SignUp from "./components/signUp";
import Home from './components/Home'
import PropTypes from 'prop-types'


const App = () => {
  const [value, setValue] = useState(!!(localStorage.getItem('token')));
  
  const closeSesion = () => {
    setValue(false)
    localStorage.removeItem('token')
  }

  const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
      value
        ? <Component {...props} handleOut={() => setValue(false)} />
        : <Redirect to='/login' />
    )} />
  )

  return(
    <Router>
    <div className="App">
      <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
          <Link className="navbar-brand" to={"/sign-in"}>Andrès Flores</Link>
          <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul className="navbar-nav ml-auto">
              {
                !value && 
                <Fragment>
                  <li className="nav-item">
                    <Link className="nav-link" to={"/sign-in"}>Login</Link>
                  </li>
                  <li className="nav-item">
                    
                    <Link className="nav-link" to={"/sign-up"}>Sign up</Link>
                  </li>
                </Fragment>
              }
              {
                value && 
                <Fragment>
                  <li className="nav-item">
                    <Link className="nav-link" onClick={() => closeSesion()} to={"/sign-in"}>Logout</Link>
                  </li>
                </Fragment>
              } 
            </ul>
          </div>
        </div>
      </nav>

      {
        <div className={value ? '' : 'auth-wrapper'}>
          <div className={value ? '' : 'auth-inner'}>
            <Switch>
              <Route exact path='/' render={(props) => <Login {...props}  handleIn={() => setValue(true)} />} />
              <Route exact path='/login' render={(props) => <Login {...props}  handleIn={() => setValue(true)} />}  />
              <Route path="/sign-in" render={(props) => <Login {...props}  handleIn={() => setValue(true)} />} />
              <Route path="/sign-up" render={(props) => <SignUp {...props} handleIn={() => setValue(true)} />} />
              <PrivateRoute path='/home' component={Home} />       
            </Switch>
          </div>
        </div>
      }
    </div>
  </Router>
  )
}

App.propTypes = {
  component: PropTypes.any,
};

export default App;
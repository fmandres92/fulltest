import React from 'react';
import PropTypes from 'prop-types'

const Character = (props) => {
    return(
        <div key={props.id} className="characterCard">
            <div className="cardHeader">
                <div className="cardImage">
                    <img src={props.image} alt={props.name} />
                </div>
                <div className="cardTitle">
                    <div className="cardName">
                        <h2 className="characterCard__Name-sc-1ejywvi-4 ieUvkm">{props.name}</h2>
                    </div>
                </div>
            </div>
            <div className="cardInfo">
                <div className="cardText">
                    <span>STATUS</span>
                    <p>{props.status}</p>
                </div>
                <div className="cardText">
                    <span>SPECIES</span>
                    <p>{props.species}</p>
                </div>
                <div className="cardText">
                    <span>GENDER</span>
                    <p>{props.gender}</p>
                </div>
            </div>
        </div>
    )
}

Character.propTypes = {
    id: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
    species: PropTypes.string.isRequired,
    gender: PropTypes.string.isRequired
};

export default Character;
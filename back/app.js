const express = require('express');
const cors = require('cors');

const app = express();
const PORT = process.env.PORT || 5000;

app.use(cors());

app.use( (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET','POST','DELETE','PUT','OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    next()
});

const auth = require('./src/routes/authRoutes');
const characters = require('./src/routes/charactersRoutes');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/api', auth);
app.use('/api', characters);

app.listen(PORT, () => {
   console.log('Server is listening on port:', PORT)
});
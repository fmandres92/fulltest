const env = process.env.REDIS || '127.0.0.1';

const redis = require('redis');

const ApiCLient = (() => {

    let instance;

    const createInstance = () => {
        const ints = redis.createClient({
            host: env,
            port: 6379
        });
        return ints;
    }

    return {
        getInstance ()  {
            if (!instance) {
                instance = createInstance();
            }
            return instance;
        }
    }
})();

module.exports = ApiCLient.getInstance();

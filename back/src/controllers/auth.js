const HttpStatus = require('http-status-codes');
const jwt = require('../utils/jwt')
const client = require('../ApiCLient')
const {promisify} = require('util');
const getAsync = promisify(client.get).bind(client);
const Cryptr = require('cryptr');
const cryptr = new Cryptr('myTotalySecretKey');

module.exports = {
    async signIn(req, res) {
        try {
            const { username, password } = req.body;

            if(!username || !password) {
                return res.status(HttpStatus.BAD_REQUEST).json({
                    message: 'Invalid credentials',
                })
            }

            const hash = await getAsync(username);

            if(!hash) {
                return res.status(HttpStatus.BAD_REQUEST).json({
                    message: 'Invalid username',
                })
            }

            let user = cryptr.decrypt(hash);
            user = JSON.parse(user);           

            if(user.username === username && user.password === password){
                const token = await jwt.setToken(hash)
                return res.status(HttpStatus.OK).json({
                    token
                })
            } else {
                return res.status(HttpStatus.BAD_REQUEST).json({
                    message: 'Invalid credentials',
                })
            }
        } catch (err) {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                message: 'Something went bad'
            })
        }
    },

    async signUp(req, res) {
        try {
            const { username, password } = req.body;

            if(!username || !password) {
                return res.status(HttpStatus.BAD_REQUEST).json({
                    message: 'Invalid credentials',
                })
            }

            const user = await getAsync(username);
            if(user) {
                return res.status(HttpStatus.BAD_REQUEST).json({
                    message: 'Username is not available',
                })
            }

            const value = JSON.stringify({username,password})
            
            const hash = cryptr.encrypt(value);

            client.set(username, hash);
            const token = await jwt.setToken(hash)

            return res.status(HttpStatus.OK).json({
                token
            })
        } catch (err) {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                message: 'Error ocurred'
            })
        }
    }
}
import React, { useState } from "react";
import {ToastsContainer, ToastsStore} from 'react-toasts';
import PropTypes from 'prop-types';
import Api from '../Api'

const Login = (props) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = () => {
    props.apiClient.login({username,password}).then( (resp) => {
      const token = resp.data.token;
      localStorage.setItem('token', token);
      props.handleIn()
      props.history.push('/home')
    }).catch((err) => {
        if(err.status === 400) {
          ToastsStore.error(err.data.message)
        } else {
          ToastsStore.error('Sorry something went bad')
        }
    })
  }

  return(
    <form>
        <h3>Sign In</h3>

        <div className="form-group">
            <label>Username</label>
            <input type="text" value={username} onChange={(e) => setUsername(e.target.value) } className="form-control" placeholder="Enter Username" />
        </div>

        <div className="form-group">
            <label>Password</label>
            <input type="password" value={password} onChange={(e) => setPassword(e.target.value) }  className="form-control" placeholder="Enter password" />
        </div>

        <button type="button" onClick={() => handleSubmit()} className="btn btn-primary btn-block">Submit</button>
        <ToastsContainer store={ToastsStore}/>
    </form>
  )
}

Login.propTypes = {
  history: PropTypes.any,
  apiClient: PropTypes.shape(),
  handleIn: PropTypes.func
};

Login.defaultProps = {
  apiClient: new Api()
}

export default Login;

const express = require('express');
const router = express.Router();

const authCtrl = require('../controllers/auth');

router.post('/signIn', authCtrl.signIn);

router.post('/signUp', authCtrl.signUp);

module.exports = router;
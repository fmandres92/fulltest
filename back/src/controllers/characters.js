const HttpStatus = require('http-status-codes');
const jwt = require('../utils/jwt')
const characters = require('../utils/fetch')

module.exports = {
    async getAll(req, res) {
        try {
            let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase
            if (token.startsWith('Bearer ')) {
              token = token.slice(7, token.length);
            }

            try {
                await jwt.validateToken(token)
            } catch (error) {
                return res.status(HttpStatus.FORBIDDEN).json({
                    message: 'Token invalid'
                })
            }
            const list = await characters.fetch();
            return res.status(HttpStatus.OK).json({ list })

        } catch (err) {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                message: 'Something went bad'
            })
        }
    },
}
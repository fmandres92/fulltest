import React, {Component} from 'react'
import PropTypes from 'prop-types'
import Api from '../Api'
import './style.css';
import Character from './character'
import { css } from "@emotion/core";
import {ToastsContainer, ToastsStore} from 'react-toasts';


import { ClipLoader } from "react-spinners";

const override = css`
  display: block;
  margin: 300px auto;
`;

export default class Home extends Component {
    constructor(props){
        super(props)
        this.state = {
            characters : [],
            loading: true
        }
        this.api = props.apiClient;
    }

    async componentDidMount() {
        try {
            const resp = await this.api.getAll();
            this.setState({
                characters: resp.data.list,
                loading: false
            })
        } catch (error) {
            if(error.status === 403) {
                ToastsStore.error(error.data.message);
                localStorage.removeItem('token');
                this.props.handleOut()
              } else {
                ToastsStore.error('Sorry something went bad')
              }
        }
    }

    renderList() {
        return(
            <ul>
                {this.state.characters.map((item)=> {
                    return ( <Character key={item.id} {...item}/> )
                })}
            </ul>
        )
    }
    render(){
        if(this.state.loading) {
            return (
                <div className="sweet-loading">
                    <ClipLoader
                        size={100}
                        color={"#123abc"}
                        loading={this.state.loading}
                        css={override}
                    />
                </div>
            );
        }
        return(
            <div className="contenCards">
                <h1 className="center">List of rick and morty characters</h1>
                {this.renderList()}
                <ToastsContainer store={ToastsStore}/>        
            </div>
        )
    }
}

Home.propTypes = {
    apiClient: PropTypes.shape(),
    handleOut: PropTypes.func.isRequired
}
  
Home.defaultProps = {
    apiClient: new Api()
}
  
